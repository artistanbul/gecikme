package com.artistanbulpr.gecikme;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Time extends GregorianCalendar {
    final static Time OPENING = new Time(9, 0);
    final static Time CLOSING = new Time(18, 0);
    final static Time LATE = new Time(9, 30);

    public Time() {
        super(TimeZone.getDefault());
    }

    public Time(int hour, int minute) {
        super(TimeZone.getDefault());
        this.set(HOUR_OF_DAY, hour);
        this.set(MINUTE, minute);
    }

    public boolean isWeekend() {
        return this.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
                this.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d", this.get(HOUR_OF_DAY), this.get(MINUTE));
    }
}
