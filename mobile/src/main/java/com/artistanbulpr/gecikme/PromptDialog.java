package com.artistanbulpr.gecikme;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class PromptDialog extends DialogFragment {
    private String title;
    private EditText editText;
    private String editTextText;
    private OnSubmitListener onSubmitListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_custom_reason, null);
        editText = (EditText) view.findViewById(R.id.reason);
        editText.setText(editTextText);
        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (onSubmitListener != null) {
                            onSubmitListener.onSubmit(editText.getText().toString());
                        }
                    }
                })
                .create();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setEditText(String text) {
        editTextText = text;
    }

    public void setOnSubmitListener(OnSubmitListener onSubmitListener) {
        this.onSubmitListener = onSubmitListener;
    }

    public interface OnSubmitListener {
        void onSubmit(String text);
    }
}
