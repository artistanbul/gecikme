package com.artistanbulpr.gecikme;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements RadioGroup.OnCheckedChangeListener, AdapterView.OnItemSelectedListener,
        TimePickerDialog.OnTimeSetListener {
    private SharedPreferences preferences;
    private Time eta;

    private Spinner reasons;
    private CheckBox compensate;
    private RadioGroup absenceTypes;
    private RadioButton absenceType;

    private DialogInterface.OnClickListener finishActivity = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            finish();
        }
    };

    public final String RECIPIENT_KEY = "recipient";
    public final String RECIPIENT_HINT = "team@artistanbulpr.com";

    String reason;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = getSharedPreferences("preferences", MODE_PRIVATE);

        reasons = (Spinner) findViewById(R.id.reasons);
        reasons.setOnItemSelectedListener(this);
        compensate = (CheckBox) findViewById(R.id.compensate);
        absenceTypes = (RadioGroup) findViewById(R.id.absenceType);
        absenceTypes.setOnCheckedChangeListener(this);
        absenceTypes.check(R.id.typeDelay);

        // Show a snackbar if weekend
        Time time = new Time();
        if (time.isWeekend()) {
            SimpleDateFormat weekdayFormat = new SimpleDateFormat("EEEE", new Locale("tr", "TR"));
            final Snackbar snackbar = Snackbar.make(
                    findViewById(R.id.coordinator),
                    String.format(
                            getString(R.string.message_weekend),
                            weekdayFormat.format(time.getTime())
                    ),
                    Snackbar.LENGTH_INDEFINITE
            );
            snackbar.setAction(android.R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        }

        // Prompt for recipient address and save it for later use.
        if (preferences.getString(RECIPIENT_KEY, null) == null) {
            PromptDialog dialog = new PromptDialog();
            dialog.setCancelable(false);
            dialog.setTitle(getString(R.string.title_recipient));
            dialog.setEditText(RECIPIENT_HINT);
            dialog.setOnSubmitListener(new PromptDialog.OnSubmitListener() {
                @Override
                public void onSubmit(String text) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(RECIPIENT_KEY, text);
                    editor.apply();
                }
            });
            dialog.show(getFragmentManager(), "dialog");
        }
    }

    public void showTimePickerDialog() {
        TimePickerDialog dialog = new TimePickerDialog(this, this, 10, 0, true);
        dialog.setTitle(getString(R.string.title_eta));
        dialog.show();
    }

    public void prepareEmail(View view) {
        if (absenceTypes.getCheckedRadioButtonId() == R.id.typeDelay) {
            showTimePickerDialog();
        } else {
            sendEmail();
        }
    }

    private void sendEmail() {
        String subject = absenceType.getText().toString();
        String body;
        if (absenceTypes.getCheckedRadioButtonId() == R.id.typeDelay) {
            String etaText = String.format(getString(R.string.mail_delay_eta), eta);
            body = String.format(
                    "%s %s%s %s",
                    reason,
                    etaText,
                    compensate.isChecked() ? " " + compensate.getText() : "",
                    getString(R.string.mail_delay_sorry)
            );
        } else {
            body = String.format(
                    "%s %s",
                    reason,
                    getString(R.string.mail_remote_available)
            );
        }
        body += "\n\n" + getString(R.string.mail_common_fyi);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {
                preferences.getString(RECIPIENT_KEY, null)
        });
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(intent);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        absenceType = (RadioButton) findViewById(checkedId);
        int reasonsRes;
        switch (checkedId) {
            case R.id.typeDelay:
                reasonsRes = R.array.reasons_delay;
                compensate.setVisibility(View.VISIBLE);
                break;
            case R.id.typeRemote:
                reasonsRes = R.array.reasons_remote;
                compensate.setVisibility(View.GONE);
                break;
            default:
                reasonsRes = R.array.reasons_delay;
                break;
        }

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, reasonsRes, android.R.layout.simple_spinner_dropdown_item
        );

        reasons.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Show a prompt dialog if "other" is selected, else get mail text for selected.
        if (position + 1 == parent.getCount()) {
            PromptDialog dialog = new PromptDialog();
            dialog.setTitle(getString(R.string.title_custom_reason));
            dialog.setOnSubmitListener(new PromptDialog.OnSubmitListener() {
                @Override
                public void onSubmit(String reason) {
                    MainActivity.this.reason = reason;
                }
            });
            dialog.show(getFragmentManager(), "dialog");
        } else {
            int stringArray;
            switch (absenceTypes.getCheckedRadioButtonId()) {
                case R.id.typeDelay:
                    stringArray = R.array.mail_delay;
                    break;
                case R.id.typeRemote:
                    stringArray = R.array.mail_remote;
                    break;
                default:
                    stringArray = R.array.mail_delay;
                    break;
            }
            reason = getResources().getStringArray(stringArray)[position];
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        eta = new Time(hourOfDay, minute);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (eta.before(Time.OPENING) || eta.after(Time.CLOSING)) {
            builder.setMessage(R.string.message_out_of_working_hours)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showTimePickerDialog();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, finishActivity)
                    .show();
        } else if (eta.before(Time.LATE)) {
            builder.setMessage(R.string.message_not_late)
                    .setNeutralButton(android.R.string.ok, finishActivity)
                    .show();
        } else if (eta.after(new Time(11, 0))) {
            builder.setMessage(R.string.message_easter_egg_late)
                    .setNeutralButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendEmail();
                        }
                    })
                    .show();
        } else {
            sendEmail();
        }
    }
}
